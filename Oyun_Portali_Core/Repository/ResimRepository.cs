﻿using Oyun_Portali_Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oyun_Portali_Data.Model;
using System.Linq.Expressions;
using Oyun_Portali_Data.DataContext;
using System.Data.Entity.Migrations;

namespace Oyun_Portali_Core.Repository
{
    public class ResimRepository : IResimRepository
    {
        private readonly OyunContext _context = new OyunContext();
        public int count()
        {
            return _context.Oyun.Count();
        }

        public void Delete(int id)
        {
            var resim = GetById(id);
            if (resim != null)
            {
                _context.Resim.Remove(resim);
            }
        }

        public Resim get(Expression<Func<Resim, bool>> expression)
        {
            return _context.Resim.FirstOrDefault(expression);
        }

        public IEnumerable<Resim> GetAll()
        {
            return _context.Resim.Select(x => x);
        }

        public Resim GetById(int id)
        {
            return _context.Resim.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable<Resim> GetMany(Expression<Func<Resim, bool>> expression)
        {
            return _context.Resim.Where(expression);
        }

        public void Insert(Resim obj)
        {
            _context.Resim.Add(obj);
        }

        public void save()
        {
            _context.SaveChanges();
        }

        public void Update(Resim obj)
        {
            _context.Resim.AddOrUpdate();
        }
    }
}
