﻿using Oyun_Portali_Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oyun_Portali_Data.Model;
using System.Linq.Expressions;
using Oyun_Portali_Data.DataContext;
using System.Data.Entity.Migrations;

namespace Oyun_Portali_Core.Repository
{
    public class OyunRepository : IOyunRepository
    {
        private readonly OyunContext _context = new OyunContext();
        public int count()
        {
            return _context.Oyun.Count();
        }

        public void Delete(int id)
        {
           var oyun = GetById(id);
            if(oyun !=null)
            {
                _context.Oyun.Remove(oyun);
            }

        }

        public Oyun get(Expression<Func<Oyun, bool>> expression)
        {
            return _context.Oyun.FirstOrDefault(expression);
        }

        public IEnumerable<Oyun> GetAll()
        {
           return _context.Oyun.Select(x=> x );
        }

        public Oyun GetById(int id)
        {
            return _context.Oyun.FirstOrDefault(x => x.ID == id );
        }

        public IQueryable<Oyun> GetMany(Expression<Func<Oyun, bool>> expression)
        {
            return _context.Oyun.Where(expression);
        }

        public void Insert(Oyun obj)
        {
            _context.Oyun.Add(obj);
        }

        public void save()
        {
            _context.SaveChanges();
        }

        public void Update(Oyun obj)
        {
            _context.Oyun.AddOrUpdate();
        }
    }
}
