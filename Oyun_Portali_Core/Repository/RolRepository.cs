﻿using Oyun_Portali_Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oyun_Portali_Data.Model;
using System.Linq.Expressions;
using Oyun_Portali_Data.DataContext;
using System.Data.Entity.Migrations;

namespace Oyun_Portali_Core.Repository
{
    public class RolRepository : IRolRepository
    {
        private readonly OyunContext _context = new OyunContext();
        public int count()
        {
            return _context.Rol.Count();
        }

        public void Delete(int id)
        {
            var rol = GetById(id);
            if (rol != null)
            {
                _context.Rol.Remove(rol);
            }
        }

        public Rol get(Expression<Func<Rol, bool>> expression)
        {
            return _context.Rol.FirstOrDefault(expression);
        }

        public IEnumerable<Rol> GetAll()
        {
            return _context.Rol.Select(x => x);
        }

        public Rol GetById(int id)
        {
            return _context.Rol.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable<Rol> GetMany(Expression<Func<Rol, bool>> expression)
        {
            return _context.Rol.Where(expression);
        }

        public void Insert(Rol obj)
        {
            _context.Rol.Add(obj);
        }

        public void save()
        {
            _context.SaveChanges();
        }

        public void Update(Rol obj)
        {
            _context.Rol.AddOrUpdate();
        }
    }
}
