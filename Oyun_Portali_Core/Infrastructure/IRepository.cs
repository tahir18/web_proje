﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Portali_Core.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        //Tüm Datamızı burada çekicez.  
        IEnumerable<T> GetAll();

        //Tek bir nesne dönücek burada.
        T GetById(int id);

        T get(Expression<Func<T, bool>> expression);

        IQueryable<T> GetMany(Expression<Func<T, bool>> expression);

        void Insert(T obj);

        void Update(T obj);

        void Delete(int id);

        int count();

        void save();


    }
}
