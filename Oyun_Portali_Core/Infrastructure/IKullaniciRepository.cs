﻿using Oyun_Portali_Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Portali_Core.Infrastructure
{
    public interface IKullaniciRepository : IRepository<Kullanici>
    {
    }
}
