namespace Oyun_Portali_Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Olustur1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Oyun",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Baslik = c.String(nullable: false, maxLength: 255),
                        KisaAciklama = c.String(),
                        Aciklama = c.String(),
                        Okunma = c.Int(nullable: false),
                        AktifMi = c.Boolean(nullable: false),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        Resim = c.String(maxLength: 255),
                        Kullanici_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Kullanici", t => t.Kullanici_ID)
                .Index(t => t.Kullanici_ID);
            
            CreateTable(
                "dbo.Resim",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ResimUrl = c.String(),
                        Oyun_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Oyun", t => t.Oyun_ID)
                .Index(t => t.Oyun_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Resim", "Oyun_ID", "dbo.Oyun");
            DropForeignKey("dbo.Oyun", "Kullanici_ID", "dbo.Kullanici");
            DropIndex("dbo.Resim", new[] { "Oyun_ID" });
            DropIndex("dbo.Oyun", new[] { "Kullanici_ID" });
            DropTable("dbo.Resim");
            DropTable("dbo.Oyun");
        }
    }
}
