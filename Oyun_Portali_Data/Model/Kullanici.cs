﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Portali_Data.Model
{
    [Table("Kullanici")]
    public   class Kullanici

    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Ad Soyad: ")]
        [MaxLength(150, ErrorMessage = "Lütfen 150 karakterden fazla değer Girmeyinizz")]
        public string AdSoyad { get; set; }

        [Display(Name = "E-mail")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Lütfen Gecerli bir mail adresi giriniz")]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Sifre")]
        [DataType(DataType.Password)]
        [MaxLength(16, ErrorMessage = "Lütfen 16 karakterden fazla değer Girmeyinizz")]
        [Required]//Zorunluluk
        public string Sifre { get; set; }

        [Display(Name = "Kayit Tarihi")]
        public DateTime KayitTarihi { get; set; }

        [Display(Name = "Aktif")]
        public bool Aktif { get; set; }

        public virtual Rol Rol { get; set; } // rol clasından baglantı alma 

    }
}
