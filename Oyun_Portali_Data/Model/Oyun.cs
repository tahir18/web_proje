﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Portali_Data.Model
{
    [Table("Oyun")]
    public class Oyun
    {
        [Key]
        public int ID { get; set; }

        [Display(Name="Oyun Baslik")]
        [MaxLength(255, ErrorMessage = "Çok Fazla Girdiniz")]
        [Required]
        public string Baslik {get;set;}

        [Display(Name = "Kisa Aciklama")]
        public string KisaAciklama { get; set; }

        [Display(Name = "Aciklama")]
        public string Aciklama { get; set; }

        public int Okunma { get; set; }

        [Display(Name = "Aktif")]
        public bool AktifMi { get; set; }

        [Display(Name = "Eklenme Tarihi")]
        public DateTime EklenmeTarihi { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        [Display(Name = "Resim")]
        [MaxLength(255, ErrorMessage = "Çok Fazla Girdiniz")]
        public string Resim { get; set; }

        public virtual ICollection<Resim> Resims { get; set; }









    }
}
