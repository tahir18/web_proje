﻿using Oyun_Portali_Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Portali_Data.DataContext
{
    public class OyunContext : DbContext
    {
        public DbSet<Kullanici> Kullanici {get;set;}
        public DbSet<Rol> Rol { get; set; }
        public DbSet<Oyun> Oyun { get; set; }
        public DbSet<Resim> Resim { get; set; }

            
    }
}
